import pygame
import random
from pygame.locals import *
import configparser

pygame.init()

# window
f = pygame.font.Font(None, 28)
startf = pygame.font.Font(None, 56)
namef = pygame.font.Font(None, 237)
window = pygame.display.set_mode((1000, 1000))
pygame.display.set_caption("My Game")

# clock
clock = pygame.time.Clock()

# player

player = pygame.image.load('racecar.png')
player = pygame.transform.scale(player, (50, 50))
player_up = player
player_down = pygame.transform.rotate(player, 180)
player_right = pygame.transform.rotate(player, 270)
player_left = pygame.transform.rotate(player, 90)
manaarth = configparser.RawConfigParser()
manaarth.read('./config.cfg')

x = manaarth.getint("info", "x")
y = manaarth.getint("info", "y")
width = manaarth.getint("info", "width")
height = manaarth.getint("info", "height")
velocity = manaarth.getint("info", "velocity")
run = manaarth.getboolean("info", "run")
playercount = manaarth.getint("info", "playercount")
screenwidth = manaarth.getint("info", "screenwidth")
temprotate = manaarth.get("info", "temprotate")
score1 = manaarth.getint("info", "score1")
score2 = manaarth.getint("info", "score2")
text1 = manaarth.get("info", "text1")
text2 = manaarth.get("info", "text2")
text3 = manaarth.get("info", "text3")
score = manaarth.get("info", "score")
level1 = manaarth.getint("info", "level1")
level2 = manaarth.getint("info", "level2")
tied = manaarth.getint("info", "tied")
time = manaarth.getint("info", "time")
timescore = manaarth.getint("info", "timescore")
up = pygame.K_UP
down = pygame.K_DOWN
left = pygame.K_LEFT
right = pygame.K_RIGHT
# obstacle
fixed_obstacle1 = pygame.image.load('obstacle_01.png')
fixed_obstacle2 = pygame.image.load('obstacle_02.png')
moving_obstacle = pygame.image.load('moving_obstacle.png')
fixed_obstacle1 = pygame.transform.scale(fixed_obstacle1, (25, 75))
fixed_obstacle2 = pygame.transform.scale(fixed_obstacle2, (25, 75))
moving_obstacle = pygame.transform.scale(moving_obstacle, (100, 75))
obstacle1x = [0, 0, 0, 0]
obstacle2x = [0, 0, 0, 0]
movvel = [20, 30, 40, 50]
level_counter = level1
view_start = True
view_result = False
scorescreen = False


def redraw():
    global x
    global y
    global player
    window.fill((128, 128, 128))
    pygame.draw.rect(window, (64, 64, 64), (0, 0, 1000, 50))
    pygame.draw.rect(window, (64, 64, 64), (0, 190, 1000, 50))
    pygame.draw.rect(window, (64, 64, 64), (0, 380, 1000, 50))
    pygame.draw.rect(window, (64, 64, 64), (0, 570, 1000, 50))
    pygame.draw.rect(window, (64, 64, 64), (0, 760, 1000, 50))
    pygame.draw.rect(window, (64, 64, 64), (0, 950, 1000, 50))
    window.blit(player, (x, y))
    if x < 0:
        x = 0
    if y < 0:
        y = 0
    if x > screenwidth-width:
        x = screenwidth-width
    if y > screenwidth-height:
        y = screenwidth-height


def collision(x1, y1, w1, h1, x2, y2, w2, h2):
    if y2+h2 >= y1 and y2 <= y1+h1 and x2+w2 >= x1 and x2 <= x1+w1:
        return True


def drawfixedobstacles():
    global obstacle1x
    global obstacle2x
    for y in range(-1, 3):
        obstacle1x[y] = random.randint(0, 470)
        obstacle2x[y] = random.randint(500, 975)


def endgamescreen():
    global view_result
    global run
    global window
    while view_result:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                view_result = False
        keysss = pygame.key.get_pressed()
        if keysss[pygame.K_RETURN]:
            view_result = False
            run = True
        elif keysss[pygame.K_q]:
            checkscore()
        window.fill((0, 0, 0))
        window.blit(namef.render("Race for Life",
                                 True, (0, 255, 255)), (0, 150))
        if score1 > score2:
            window.blit(startf.render("Player 1 wins ",
                                      True, (255, 255, 255)), (390, 472))
        elif score2 > score1:
            window.blit(startf.render("Player 2 wins ",
                                      True, (255, 255, 255)), (390, 472))
        else:
            window.blit(startf.render("Its a Tie ", True,
                                      (255, 255, 255)), (410, 472))
        window.blit(startf.render("Press Enter to Continue Game",
                                  True, (255, 255, 255)), (260, 600))
        pygame.display.update()


def gameover():
    global x
    global y
    global playercount
    global text1
    global text2
    global text3
    global score
    global score1
    global score2
    global time
    global level_counter
    global level2
    global level1
    global player
    global player_up
    global player_down
    global view_result
    global run
    global window
    global up
    global down
    global right
    global left
    global tied
    time = 0
    temp = text1
    text1 = text2
    text2 = temp

    if playercount == 1:
        x = 475
        y = 0
        playercount = 2
        player = player_down
        text3 = "Player2"
        score1 = int(score)
        up = pygame.K_w
        down = pygame.K_s
        left = pygame.K_a
        right = pygame.K_d

    else:
        x = 475
        y = 950
        playercount = 1
        player = player_up
        text3 = "Player1"
        score2 = int(score)
        view_result = True
        run = False
        up = pygame.K_UP
        down = pygame.K_DOWN
        left = pygame.K_LEFT
        right = pygame.K_RIGHT
        if score1 > score2:
            level1 += 1
        elif score2 > score1:
            level2 += 1
        elif score1 == score2:
            level1 += 1
            level2 += 1
            tied += 1
    if playercount == 1:
        level_counter = level1
    else:
        level_counter = level2
    endgamescreen()


class Movingobstacles:

    def __init__(self, x, y, vel):
        self.x = x
        self.y = y
        self.vel = vel

    def update(self):
        global level_counter
        self.x += self.vel + (level_counter-1)*10
        if self.x > screenwidth:
            self.x = -self.vel
        if collision(self.x, self.y, 100, 75, x, y, 50, 50):
            gameover()


def scoring1(y1):
    global score
    if y1 == 0:
        score = '170'
    elif y1 > 0:
        score = '160'
    elif y1 > 140:
        score = '155'
    if y1 > 190:
        score = '145'
    if y1 > 330:
        score = '140'
    if y1 > 380:
        score = '130'
    if y1 > 520:
        score = '125'
    if y1 > 570:
        score = '115'
    if y1 > 710:
        score = '110'
    if y1 > 760:
        score = '100'


def scoring2(y1):
    global score
    if y1 >= 0:
        score = '100'
    if y1 > 190:
        score = '110'
    if y1 > 240:
        score = '115'
    if y1 > 380:
        score = '125'
    if y1 > 430:
        score = '130'
    if y1 > 570:
        score = '140'
    if y1 > 620:
        score = '145'
    if y1 > 760:
        score = '155'
    if y1 > 810:
        score = '160'
    if y1 > 950:
        score = '170'


def checkscore():
    global scorescreen
    global run
    scorescreen = True
    run = False
    while scorescreen:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                scorescreen = False
        keyssss = pygame.key.get_pressed()
        if keyssss[pygame.K_RETURN]:
            scorescreen = False
            run = True
        window.fill((0, 0, 0))
        window.blit(startf.render("Press Enter to Start Game",
                                  True, (255, 255, 255)), (280, 472))
        window.blit(startf.render("Score:- Player1 | Player2",
                                  True, (255, 255, 255)), (250, 200))
        window.blit(startf.render(str(level1) + '|' + str(level2),
                                  True, (255, 255, 255)), (520, 250))
        pygame.display.update()


while view_start:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            view_start = False
    keyss = pygame.key.get_pressed()
    if keyss[pygame.K_RETURN]:
        view_start = False
        run = True
    elif keyss[pygame.K_q]:
        checkscore()
    window.fill((0, 0, 0))
    window.blit(namef.render("Race for Life", True, (0, 255, 255)), (0, 150))
    window.blit(startf.render("Press Enter to Start Game",
                              True, (255, 255, 255)), (280, 472))
    window.blit(f.render("Use arrow keys for Player1 and W,A,S,D for Player2.",
                         True, (255, 255, 255)), (120, 800))
    window.blit(f.render("Press Q to check the score",
                         True, (255, 255, 255)), (600, 800))

    pygame.display.update()


mov_list = []
for i in range(10):
    mov_list += [Movingobstacles(random.randint(0, 1000), random.choice(
        [190, 380, 570, 760, 950])-105, random.randint(2, 20))]
drawfixedobstacles()
while run:
    clock.tick(25)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    keys = pygame.key.get_pressed()

    if keys[left]:
        x -= velocity
        player = player_left
    elif keys[right]:
        x += velocity
        player = player_right
    elif keys[up]:
        y -= velocity
        player = player_up
    elif keys[down]:
        y += velocity
        player = player_down
    elif keys[pygame.K_q]:
        checkscore()
    redraw()

    i = 0
    for y1 in [190, 380, 570, 760]:
        window.blit(fixed_obstacle1, (obstacle1x[i], y1-25))
        window.blit(fixed_obstacle2, (obstacle2x[i], y1-25))
        if collision(obstacle1x[i], y1, 25, 50, x, y, 50, 50):
            gameover()
        if collision(obstacle2x[i], y1, 25, 50, x, y, 50, 50):
            gameover()
        i = i+1

    for obstc in mov_list:
        window.blit(moving_obstacle, (obstc.x, obstc.y))
        obstc.update()

    window.blit(f.render(text1, True, (255, 255, 64)),
                (screenwidth/2, 1000-20))
    window.blit(f.render(text2, True, (255, 255, 64)), (screenwidth/2, 0))
    window.blit(f.render(text3, True, (255, 255, 255)), (0, 0))
    window.blit(f.render("Level " + str(level_counter),
                         True, (255, 255, 255)), (0, 25))

    time += 1

    if playercount == 1:
        scoring1(y)
    elif playercount == 2:
        scoring2(y)
    score = str(int(score)-time//100)
    window.blit(f.render("Score= " + score, True, (255, 255, 255)), (890, 0))

    if playercount == 1 and y < 50:
        score = str(int(score)+10)
        gameover()
    elif playercount == 2 and y > 900:
        score = str(int(score)+10)
        gameover()
    pygame.display.update()
pygame.quit()
