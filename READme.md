# Race For Life

----
## How to run

1. Fork the repository.
2. Clone in your local machine.
3. Install python3 using the command 'sudo apt install python3'
4. Run the game using the command 'python3 app.py'
----
## How to play
1. Press Enter to start the game.
2. Use arrow keys to move Player1 and W,A,S,D to move Player2.
3. Initial score is 100 and it decreases as time passes.
4. Crossing a fixed obstacle adds 5 to the score.
5. Crossing a random obstacle adds 10 to the score.
6. Press Q to pause and check the score at anytime.
----

## Developers
Developed by one and only Manaarth Saini with little amount of sleep.

